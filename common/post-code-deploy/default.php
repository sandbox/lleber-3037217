#!/usr/bin/php
<?php

use Drupal\cloudhooks\Event\PostCodeDeployEvent;

require_once dirname(dirname(dirname(__FILE__))) . '/include/bootstrap.php';

global $argv;

list($application, $environment, $source_branch, $deployed_tag, $repo_url, $repo_type) = array_slice($argv, 1);

$event = new PostCodeDeployEvent(
  $application,
  $environment,
  $source_branch,
  $deployed_tag,
  $repo_url,
  $repo_type
);

/* @var $event_dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
$event_dispatcher = \Drupal::service('event_dispatcher');

$event_dispatcher->dispatch(
  PostCodeDeployEvent::POST_CODE_DEPLOY,
  $event
);
