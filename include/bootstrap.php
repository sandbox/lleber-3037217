<?php

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

$autoload_file = 'vendor/autoload.php';

if (!file_exists($autoload_file) || !is_readable($autoload_file)) {
  exit;
}

/* @noinspection PhpIncludeInspection */
$autoloader = require_once $autoload_file;

$request = Request::createFromGlobals();

$kernel = DrupalKernel::createFromRequest($request, $autoloader, 'prod');
$kernel->boot();
