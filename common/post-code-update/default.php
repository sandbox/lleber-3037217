#!/usr/bin/php
<?php

use Drupal\cloudhooks\Event\PostCodeUpdateEvent;

require_once dirname(dirname(dirname(__FILE__))) . '/include/bootstrap.php';

global $argv;

list($application, $environment, $source_branch, $deployed_tag, $repo_url, $repo_type) = array_slice($argv, 1);

$event = new PostCodeUpdateEvent(
  $application,
  $environment,
  $source_branch,
  $deployed_tag,
  $repo_url,
  $repo_type
);

/* @var $event_dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
$event_dispatcher = \Drupal::service('event_dispatcher');

$event_dispatcher->dispatch(
  PostCodeUpdateEvent::POST_CODE_UPDATE,
  $event
);
