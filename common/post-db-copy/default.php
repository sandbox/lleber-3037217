#!/usr/bin/php
<?php

use Drupal\cloudhooks\Event\PostDatabaseCopyEvent;

require_once dirname(dirname(dirname(__FILE__))) . '/include/bootstrap.php';

global $argv;

list($application, $environment, $database_name, $source_environment) = array_slice($argv, 1);

$event = new PostDatabaseCopyEvent(
  $application,
  $environment,
  $database_name,
  $source_environment
);

/* @var $event_dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
$event_dispatcher = \Drupal::service('event_dispatcher');

$event_dispatcher->dispatch(
  PostDatabaseCopyEvent::POST_DB_COPY,
  $event
);
