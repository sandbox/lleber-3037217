#!/usr/bin/php
<?php

use Drupal\cloudhooks\Event\PostFilesCopyEvent;

require_once dirname(dirname(dirname(__FILE__))) . '/include/bootstrap.php';

global $argv;

list($application, $environment, $database_name, $source_environment) = array_slice($argv, 1);

$event = new PostFilesCopyEvent(
  $application,
  $environment,
  $source_environment
);

/* @var $event_dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
$event_dispatcher = \Drupal::service('event_dispatcher');

$event_dispatcher->dispatch(
  PostFilesCopyEvent::POST_FILES_COPY,
  $event
);
